# README #

I've been using Docker for laravel, node, and vue development for a while now, but I wanted to 
create a boilerplate environment for starting new projects.  This starts with Laravel 5.5LTS, running
under Nginx and PHP7.2-FPM.  

Each service will be container independent.  In otherwords, each service will reside on it's own 
container.  Nginx is one container.  PHP another, separate container.  MySQL/MariaDB, etc. you get the idea.
My end goal is to create a series of containers for an application that can be deployed via docker containers.

You should have 4 containers, 
* jwilder/nginx-proxy to route without going to host:port  
* nginx web server
* mysql database, could be replaced with any SQL or NOSQL variant
* PHP7.1-FPM for executing PHP code.

### Laravel Docker dev & deployment ###

* A laravel development and docker deployment environment

### How do I get set up? ###

Depending on how you got this, if you did a git fork, then the following should get you started
for developing in Laravel 5.5LTS.

1. edit docker-compose.yml

The biggest component in here is under Nginx config, set Remember Google Chrome will force https on .dev TLDs so choose wisely.

2. edit conf/nginx/vhost.conf
   you also need to set your hostname in nginx, vi conf/nginx/vhost.conf and look for the line ServerName example.com and change to your desired domain name, should 100% match docker-compose virtual_host value for routing to work.

3. you will need a .env file for laravel to work. Copy the env.example or .env.sample file.  env.example is my example env file and run php artisan key:generate to provide an access key
4. docker-compose build
5. docker-compose up -d
6. point browser to http://<your dev site>


### Contribution guidelines ###

Right now, I don't know if there will be any contributions.  If someone forks the repo and comes up with something, submit a PR and I'd consider it after being happily surprised.
